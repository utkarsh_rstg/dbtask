FROM openjdk:8
EXPOSE 8083
ADD target/dbtask.jar dbtask.jar
ENTRYPOINT ["java","-jar","/dbtask.jar"]