package com.centime.dbtask.controllers;

import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.centime.dbtask.annotations.LogMethodParam;
import com.centime.dbtask.models.CustomPerson;
import com.centime.dbtask.models.Person;
import com.centime.dbtask.services.interfaces.IPersonService;

@RestController
public class ServiceController {

	private static final Logger logger = LoggerFactory.getLogger(ServiceController.class);

	@Autowired
	IPersonService personService;

	@LogMethodParam
	@GetMapping("/person/{id}")
	public ResponseEntity<Person> getPersonById(@PathVariable("id") Integer id) {

		Person person = null;
		try {
			person = personService.getPersonById(id);

		} catch (NoSuchElementException e) {

			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

		}

		return new ResponseEntity<Person>(person, HttpStatus.OK);
	}

	@GetMapping("/persons")
	public ResponseEntity<List<Person>> getAllPersons() {
		List<Person> person = null;
		try {
			person = personService.getAllPersonList();

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
		return new ResponseEntity<List<Person>>(person, HttpStatus.OK);
	}
	
	@GetMapping("/personsByHierarchy")
	public ResponseEntity<List<CustomPerson>> getAllPersonsByHierarchy() {
		List<CustomPerson> person = null;
		try {
			person = personService.getAllPersonListByHierarchy();

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
		return new ResponseEntity<List<CustomPerson>>(person, HttpStatus.OK);
	}

	@PostMapping("/person")
	@LogMethodParam
	public ResponseEntity<Person> savePerson(@RequestBody Person person) {
		try {
			person = personService.savePerson(person);

		} catch (Exception e) {

			logger.error("Error Occured in saving person Response--input->", person);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
		return new ResponseEntity<Person>(person, HttpStatus.OK);

	}

}
