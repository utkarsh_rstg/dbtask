package com.centime.dbtask.models;

import java.util.List;

public class CustomPerson {

	
	public CustomPerson(String name ,List<CustomPerson> list) {
		this.name= name;
		this.childlist= list;
	}
	
	private String name;
	private List<CustomPerson> childlist ;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CustomPerson> getChildlist() {
		return this.childlist;
	}

	public void setChildlist(List<CustomPerson> childlist) {
		this.childlist = childlist;
	}
	

}
