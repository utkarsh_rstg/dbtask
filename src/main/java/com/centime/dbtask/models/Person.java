package com.centime.dbtask.models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Person {

	@Id
	private int id;

	private int parentId;
	private String name;
	private String color;

	public Person() {

	}

	@Override
	public int hashCode() {
		return this.id;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == this)
			return true;

		if (!(obj instanceof Person)) {
			return false;
		}

		return this.id == ((Person) obj).getId();
	}

	public Person(int id, int parentId, String name, String color) {
		this.id = id;
		this.parentId = parentId;
		this.name = name;
		this.color = color;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "id-" + this.id + " " + "parentId-" + this.parentId + " " + "name-" + this.name + " " + "color-"
				+ this.color;
	}

}
