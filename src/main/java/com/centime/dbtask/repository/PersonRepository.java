package com.centime.dbtask.repository;

import org.springframework.data.repository.CrudRepository;
import com.centime.dbtask.models.Person;

public interface PersonRepository extends CrudRepository<Person, Integer> {

}
