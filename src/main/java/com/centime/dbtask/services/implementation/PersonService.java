package com.centime.dbtask.services.implementation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centime.dbtask.models.CustomPerson;
import com.centime.dbtask.models.Person;
import com.centime.dbtask.repository.PersonRepository;
import com.centime.dbtask.services.interfaces.IPersonService;

@Service
public class PersonService implements IPersonService {

	@Autowired
	PersonRepository personRepository;

	@Override
	public List<Person> getAllPersonList() {
		List<Person> personList = new ArrayList<>();
		personRepository.findAll().forEach(personList::add);
		return personList;
	}

	@Override
	public Person getPersonById(int id) {
		return personRepository.findById(id).get();
	}

	@Override
	public Person savePerson(Person person) {
		return personRepository.save(person);
	}

	@Override
	public List<CustomPerson> getAllPersonListByHierarchy() {
		List<Person> personListFromDB = new ArrayList<>();
		personRepository.findAll().forEach(personListFromDB::add);
		List<CustomPerson> customList = getPersonListByHierachy(personListFromDB);

		return customList;
	}

	private List<CustomPerson> getPersonListByHierachy(List<Person> personListFromDB) {

		Map<Integer, Person> childParentMap = new HashMap<>();
		for (Person person : personListFromDB) {
			childParentMap.put(person.getId(), person);

		}
		TreeMap<Integer, CustomPerson> hierarchyListParentMap = new TreeMap<>(); // map for parent and it is reference
		/* Map<Integer,CustomPerson> childReferenceMap = new HashMap<>(); */

		for (Map.Entry<Integer, Person> childEntry : childParentMap.entrySet()) {
			int parentId = childEntry.getValue().getParentId();
			CustomPerson childItem = new CustomPerson(childEntry.getValue().getName(), new ArrayList<>()) ;
			if (hierarchyListParentMap.get(parentId) == null) // Parent Doesn't exist on new Map on child Map
			{
				List<CustomPerson> childList = new ArrayList<>();
				childList.add(childItem);
				hierarchyListParentMap.put(childEntry.getKey(), childItem); //store Child Reference also
				String parentName = childParentMap.get(parentId) != null ? childParentMap.get(parentId).getName(): null;
				CustomPerson customPerson = new CustomPerson(parentName, childList);
				hierarchyListParentMap.put(parentId, customPerson);
			}

			/*
			 * else if (hierarchyListParentMap.get(parentId) == null &&
			 * childReferenceMap.get(parentId)!=null) //case when first Time Child Comes
			 * which is also parent
			 * 
			 * { childReferenceMap.get(childEntry.getKey()).getChildlist().add(childItem); }
			 */
			else
				
			{
				CustomPerson parent = hierarchyListParentMap.get(parentId); // get the parent and
																		// Add Child to it.
				hierarchyListParentMap.put(childEntry.getKey(), childItem);
				parent.getChildlist().add(childItem);
				

			}

		}

		return hierarchyListParentMap.firstEntry().getValue().getChildlist();
	}

}
