package com.centime.dbtask.services.interfaces;

import java.util.List;

import com.centime.dbtask.models.CustomPerson;
import com.centime.dbtask.models.Person;

public interface IPersonService {

	public List<Person> getAllPersonList();

	public Person getPersonById(int id);

	public Person savePerson(Person person);

	public List<CustomPerson> getAllPersonListByHierarchy();

}
